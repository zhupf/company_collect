﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 公司采集
{
    public class MapData
    {
        public string name = "";
        public string tel = "";
        public string addr = "";
        public string uid = "";
    }
    public class MapDataResult
    {
        public int aladdin_res_num = 0;
    }
    public class BaiduMapData
    {
        public List<MapData> content;
        public MapDataResult result;
    }
}
