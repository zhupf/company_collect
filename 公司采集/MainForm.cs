﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using LitJson;

namespace 公司采集
{
    public partial class MainForm : Form
    {
        public static string companytype = "";
        public static string location = "";
        public static string baidu_wd = "";
        public static string pathroot = "";
        public static string u_loc = "";
        public static string block_pt = "";
        public static int aladdin_res_num = 0;
        public static WebBrowser web = null;
        public static bool isUserAgentSet = false;
        public static bool isGetCompanyData = false;
        public MainForm()
        {
            pathroot = System.Windows.Forms.Application.StartupPath + "\\";
            InitializeComponent();
            web = new WebBrowser();
            web.Navigating += SetupBrowser;
        }
        void SetupBrowser(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (!isUserAgentSet)
            {
                e.Cancel = true;
                isUserAgentSet = true;
                web.Navigate(e.Url, e.TargetFrameName, null, "User-Agent: Mozilla/4.0 (compatible; MSIE 9.0; qdesk 2.5.1269.201; Windows NT 6.1; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)\r\n");
            }
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            this.listView_data.Items.Clear();
            companytype = textBox_companytype.Text;
            location = textBox_location.Text;
            if (string.IsNullOrEmpty(companytype))
            {
                companytype = "装修公司";
            }
            if (string.IsNullOrEmpty(location))
            {
                location = "杭州";
            }
            baidu_wd = UrlEncode(companytype + " " + location);
            GetCityLocation();
        }
        public static string UrlEncode(string str)
        {
            return Uri.EscapeDataString(str);
        }
        public static string UrlDecode(string str)
        {
            return Uri.UnescapeDataString(str);
        }
        public void GetCityLocation()
        {
            GetCompanyDataBegin();
            string url = "http://map.baidu.com/?newmap=1&ie=utf-8&s=s%26wd%3D" + baidu_wd;
            //http://map.baidu.com/?newmap=1&ie=utf-8&s=s%26wd%3D%E8%A3%85%E4%BF%AE%E5%85%AC%E5%8F%B8%20%E6%9D%AD%E5%B7%9E
            web.Navigate(url);
            web.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(
            delegate(object sender, WebBrowserDocumentCompletedEventArgs e)
            {
                WebBrowser websender = (WebBrowser)sender;
                if (websender.ReadyState != WebBrowserReadyState.Complete)
                {
                    return;
                }
                if (e.Url.ToString() == websender.Url.ToString())
                {
                    // "location":{"lat":3505666,"lng":13383787}
                    string reg_str = @"""location"":\{""lat"":(\d*),""lng"":(\d*)}";
                    Regex reg = new Regex(reg_str);
                    var mat = reg.Match(websender.DocumentText);
                    if (mat.Groups.Count == 3)
                    {
                        if (!isGetCompanyData)
                        {
                            isGetCompanyData = true;
                            u_loc = mat.Groups[2] + "," + mat.Groups[1];
                            block_pt = "(" + u_loc + ";" + u_loc + ")";
                            Thread t = new Thread(new ThreadStart(GetCompanyDataThread));
                            t.Start();
                        }
                    }
                    try
                    {
                        web.Stop();
                    }
                    catch (System.Exception ex)
                    {
                    }
                }
            });
        }
        public void GetCompanyDataThread()
        {
            try
            {
                GetCompanyCount();
            }
            catch (System.Exception ex)
            {
            }
            GetCompanyDataEnd();
        }
        private void GetCompanyDataBegin()
        {
            this.button_start.Enabled = false;
            isUserAgentSet = false;
            isGetCompanyData = false;
        }
        public delegate void updatestartbtn(string text, ContentAlignment align, bool enabled);
        private void UpdateStartBtn(string text, ContentAlignment align, bool enabled)
        {
            this.button_start.Enabled = enabled;
            this.button_start.TextAlign = align;
            this.button_start.Text = text;
        }
        private void GetCompanyDataEnd()
        {
            if (!this.button_start.IsHandleCreated)
            {
                return;
            }
            this.button_start.BeginInvoke(new updatestartbtn(UpdateStartBtn), new object[] { "开始采集", ContentAlignment.MiddleCenter, true });
        }
        private void BeginUpdateStartBtn(string text, ContentAlignment align, bool enabled)
        {
            this.button_start.Enabled = enabled;
            this.button_start.TextAlign = align;
            this.button_start.Text = text;
        }
        public delegate void addlistview(List<MapData> kMapDatas);
        private void AddListView(List<MapData> kMapDatas)
        {
            this.listView_data.BeginUpdate();
            foreach (var v in kMapDatas)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = v.name;
                lvi.SubItems.Add(v.tel);
                lvi.SubItems.Add(v.addr);
                this.listView_data.Items.Add(lvi);
            }
            this.listView_data.EndUpdate();
        }
        private void BeginAddListView(List<MapData> kMapDatas)
        {
            if (!this.listView_data.IsHandleCreated)
            {
                return;
            }
            this.button_start.BeginInvoke(new addlistview(AddListView), new object[] { kMapDatas });
        }

        public void GetCompanyCount()
        {
            string path_ret = pathroot + "采集结果_" + location + ".csv";
            //http://map.baidu.com/?newmap=1&reqflag=pcmap&biz=1&from=webmap&da_par=direct&pcevaname=pc4.1&qt=s&wd=%E8%A3%85%E4%BF%AE%E5%85%AC%E5%8F%B8%20%E6%9D%AD%E5%B7%9E&c=179&tn=B_NORMAL_MAP&nn=0&u_loc=13384432,3507313&ie=utf-8&l=12&b=(13348464,3492977;13445872,3521649)&t=1475946056451
            string url = "http://map.baidu.com/?newmap=1&reqflag=pcmap&biz=1&from=webmap&da_par=direct&pcevaname=pc4.1&qt=s&wd=" + baidu_wd +
                "&c=179&tn=B_NORMAL_MAP&nn=0&u_loc=" + u_loc + "&ie=utf-8&l=12&b=" + block_pt + "&t=" + DateTime.Now.Ticks;

            WebClient webClient = new WebClient();
            Byte[] pageData = webClient.DownloadData(url);
            string pageHtml = Encoding.UTF8.GetString(pageData); //如果获取网站页面采用的是UTF-8，则使用这句

            var sw = new StreamWriter(path_ret, false, Encoding.GetEncoding("GBK"));

            string data_save = "";
            
            BaiduMapData kBaiduMapData = JsonMapper.ToObject<BaiduMapData>(pageHtml);
            if (kBaiduMapData != null)
            {
                aladdin_res_num = kBaiduMapData.result.aladdin_res_num;
                if (aladdin_res_num > 0)
                {
                    int pagecount = aladdin_res_num / 10;
                    if (aladdin_res_num % 10 != 0)
                    {
                        pagecount++;
                    }
                    string wait = "...";
                    for (int pageidx = 0; pageidx < pagecount; pageidx++)
                    {
                        BeginUpdateStartBtn("采集中" + wait.Substring(pageidx % 3), ContentAlignment.MiddleLeft, false);
                        List<MapData> kMapDatas = GetCompanyData(pageidx);
                        foreach (var v in kMapDatas)
                        {
                            data_save += v.name.Replace(',', ' ') + "," + v.tel.Replace(',', ' ') + "," + v.addr.Replace(',', ' ') + "\r\n";
                        }
                    }
                }
            }
            if (data_save != "")
            {
                data_save = "公司,电话,地址\r\n" + data_save;
                sw.Write(data_save);
                sw.Close();
            }
        }
        public List<MapData> GetCompanyData(int pageIdx)
        {
            List<MapData> kMapDatas = new List<MapData>();

            string url = "http://map.baidu.com/?newmap=1&reqflag=pcmap&biz=1&from=webmap&da_par=direct&pcevaname=pc4.1&qt=s&wd=" + baidu_wd +
                "&c=179&tn=B_NORMAL_MAP&pn=" + pageIdx + "&nn=" + (pageIdx * 10) + "&u_loc=" + u_loc + "&ie=utf-8&l=12&b=" + block_pt + "&t=" + DateTime.Now.Ticks;

            WebClient webClient = new WebClient();
            Byte[] pageData = webClient.DownloadData(url);
            string pageHtml = Encoding.UTF8.GetString(pageData);

            try
            {
                BaiduMapData kBaiduMapData = JsonMapper.ToObject<BaiduMapData>(pageHtml);
                if (kBaiduMapData != null)
                {
                    kMapDatas = kBaiduMapData.content;
                    BeginAddListView(kMapDatas);
                }
            }
            catch (System.Exception ex)
            {
            }
            return kMapDatas;
        }
    }
}
